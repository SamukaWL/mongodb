const mongoose = require("mongoose");
const articleModel = require("./article");

mongoose.connect("mongodb://localhost:27017/aprendendoMongo", 
    {useNewUrlParser: true, useUnifiedTopology: true}
);

const Article = mongoose.model("Article", articleModel);

// const artigo = new Article({
//     title: "React do zero",
//     author: "samuka",
//     body: "fiajfisjaifjasifa dsauhfas",
//     special: true,
//     resumo: {
//         content: "bla bla",
//         author: "teste"
//     }
// });

// artigo.save().then(() => {
//     console.log("Artigo salvo!");
// }).catch(err => {
//     console.log(err);
// });

// Article.find({}).then(articles => {
    
//     console.log(articles);

// }).catch(err => {
//     console.log(artigos);
// });

// Article.find({'author': "samuka", 'resumo.content': "bla bla"}).then(article => {
//     console.log(article);
// }).catch(err => {
//     console.log(err)
// })

// Article.findByIdAndDelete("60ddeb5ab92ee716fca1e78c").then(() => {
//     console.log("removido");
// }).catch(err => {
//     console.log(err);
// })

Article.findByIdAndUpdate("60ddec87dcca260644dd9865", {
    title: "Node.js",
    author: "Wesley",
    body: "lorem ipsum",
}).then(() => {
    console.log("Atualizado")
}).catch(err => {
    console.log(err);
})

